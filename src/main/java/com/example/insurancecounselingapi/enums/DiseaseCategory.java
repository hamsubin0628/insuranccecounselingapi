package com.example.insurancecounselingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DiseaseCategory {
    CANCER("암",false),
    BRAIN("뇌혈관", false),
    HEART("심장", false),
    ETC("기타", true),
    NONE("질병없음", true);

    public final String diseaseCategoryEnum;
    public final Boolean join;
}
