package com.example.insurancecounselingapi.repository;


import com.example.insurancecounselingapi.entity.InsuranceCounseling;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InsuranceCounselingRepository extends JpaRepository<InsuranceCounseling, Long> {
}
