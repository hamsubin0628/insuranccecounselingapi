package com.example.insurancecounselingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsuranceCounselingApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(InsuranceCounselingApiApplication.class, args);
    }

}
