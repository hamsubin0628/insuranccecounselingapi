package com.example.insurancecounselingapi.model;

import com.example.insurancecounselingapi.enums.DiseaseCategory;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class InsuranceCounselingItem {
    private Long id;
    private LocalDate counselingDate;
    private String clientName;
    private LocalDate birthDate;
    private String phoneNumber;
    private String address;
    private Boolean contract;
    private Double hopeMoney;
    private String diseaseCategory;
    private Boolean differentJoin;
}
