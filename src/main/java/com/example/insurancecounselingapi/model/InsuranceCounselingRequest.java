package com.example.insurancecounselingapi.model;

import com.example.insurancecounselingapi.enums.DiseaseCategory;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Getter
@Setter
public class InsuranceCounselingRequest {

    private LocalDate counselingDate;

    private String clientName;

    private LocalDate birthDate;

    private String phoneNumber;

    private String address;

    private Boolean contract;

    private Double hopeMoney;

    @Enumerated(value = EnumType.STRING)
    private DiseaseCategory diseaseCategory;

    private Boolean differentJoin;
}
