package com.example.insurancecounselingapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class InsuranceCounselingResponse {
    private Long id;
    private LocalDate counselingDate;
    private String clientName;
    private LocalDate birthDate;
    private String phoneNumber;
    private String address;
    private String  contractName;
    private Double hopeMoney;
    private String diseaseCategoryName;
    private String diseaseCategoryJoin;
    private String differentJoinName;
}
