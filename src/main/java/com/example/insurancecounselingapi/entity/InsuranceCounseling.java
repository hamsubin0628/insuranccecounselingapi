package com.example.insurancecounselingapi.entity;

import com.example.insurancecounselingapi.enums.DiseaseCategory;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class InsuranceCounseling {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate counselingDate;

    @Column(nullable = false, length = 20)
    private String clientName;

    @Column(nullable = false)
    private LocalDate birthDate;

    @Column(nullable = false, length = 15)
    private String phoneNumber;

    @Column(nullable = false, length = 50)
    private String address;

    @Column(nullable = false)
    private Boolean contract;

    @Column(nullable = false)
    private Double hopeMoney;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private DiseaseCategory diseaseCategory;

    @Column(nullable = false)
    private Boolean differentJoin;
}
