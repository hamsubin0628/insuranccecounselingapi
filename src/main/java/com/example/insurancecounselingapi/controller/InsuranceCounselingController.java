package com.example.insurancecounselingapi.controller;

import com.example.insurancecounselingapi.model.InsuranceCounselingItem;
import com.example.insurancecounselingapi.model.InsuranceCounselingRequest;
import com.example.insurancecounselingapi.model.InsuranceCounselingResponse;
import com.example.insurancecounselingapi.service.InsuranceCounselingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/insurance")
public class InsuranceCounselingController {
    private final InsuranceCounselingService insuranceCounselingService;

    @PostMapping("/counseling")
    public String setInsuranceCounseling(@RequestBody InsuranceCounselingRequest request){
        insuranceCounselingService.setInsuranceCounseling(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<InsuranceCounselingItem> getInsuranceCounselings(){
        return insuranceCounselingService.getInsuranceCounselings();
    }

    @GetMapping("/detail/{id}")
    public InsuranceCounselingResponse getInsuranceCounseling(@PathVariable long id){
        return insuranceCounselingService.getInsuranceCounseling(id);
    }
}
