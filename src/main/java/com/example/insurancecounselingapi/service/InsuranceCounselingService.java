package com.example.insurancecounselingapi.service;

import com.example.insurancecounselingapi.entity.InsuranceCounseling;
import com.example.insurancecounselingapi.model.InsuranceCounselingItem;
import com.example.insurancecounselingapi.model.InsuranceCounselingRequest;
import com.example.insurancecounselingapi.model.InsuranceCounselingResponse;
import com.example.insurancecounselingapi.repository.InsuranceCounselingRepository;
import jakarta.persistence.Id;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InsuranceCounselingService {
    private final InsuranceCounselingRepository insuranceCounselingRepository;

    public void setInsuranceCounseling(InsuranceCounselingRequest insuranceCounselingRequest){
        InsuranceCounseling addData = new InsuranceCounseling();
        addData.setCounselingDate(insuranceCounselingRequest.getCounselingDate());
        addData.setClientName(insuranceCounselingRequest.getClientName());
        addData.setBirthDate(insuranceCounselingRequest.getBirthDate());
        addData.setPhoneNumber(insuranceCounselingRequest.getPhoneNumber());
        addData.setAddress(insuranceCounselingRequest.getAddress());
        addData.setContract(insuranceCounselingRequest.getContract());
        addData.setHopeMoney(insuranceCounselingRequest.getHopeMoney());
        addData.setDiseaseCategory(insuranceCounselingRequest.getDiseaseCategory());
        addData.setDifferentJoin(insuranceCounselingRequest.getDifferentJoin());

        insuranceCounselingRepository.save(addData);
    }

    public List<InsuranceCounselingItem> getInsuranceCounselings() {
        List<InsuranceCounseling> originList = insuranceCounselingRepository.findAll();

        List<InsuranceCounselingItem> result = new LinkedList<>();

        for (InsuranceCounseling insuranceCounseling : originList) {
            InsuranceCounselingItem addItem = new InsuranceCounselingItem();
            addItem.setId(insuranceCounseling.getId());
            addItem.setCounselingDate(insuranceCounseling.getCounselingDate());
            addItem.setClientName(insuranceCounseling.getClientName());
            addItem.setBirthDate(insuranceCounseling.getBirthDate());
            addItem.setPhoneNumber(insuranceCounseling.getPhoneNumber());
            addItem.setContract(insuranceCounseling.getContract());
            addItem.setHopeMoney(insuranceCounseling.getHopeMoney());
            addItem.setDiseaseCategory(insuranceCounseling.getDiseaseCategory().getDiseaseCategoryEnum());
            addItem.setDifferentJoin(insuranceCounseling.getDifferentJoin());

            result.add(addItem);
        }
        return result;
    }

    public InsuranceCounselingResponse getInsuranceCounseling(long Id){
        InsuranceCounseling originData = insuranceCounselingRepository.findById(Id).orElseThrow();

        InsuranceCounselingResponse response = new InsuranceCounselingResponse();
        response.setId(originData.getId());
        response.setCounselingDate(originData.getCounselingDate());
        response.setClientName(originData.getClientName());
        response.setBirthDate(originData.getBirthDate());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setContractName(originData.getClientName());
        response.setHopeMoney(originData.getHopeMoney());
        response.setDiseaseCategoryName(originData.getDiseaseCategory().getDiseaseCategoryEnum());
        response.setDiseaseCategoryJoin(originData.getDiseaseCategory().getJoin()? "가입불가":"가입가능");
        response.setDifferentJoinName(originData.getDifferentJoin()? "있음":"없음");

        return response;
    }
}
